package com.alina;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.Driver;


/**
 * Реализовать ProductsRepository
 *
 * - List<Product> findAll();
 * - List<Product> findAllByPrice(double price);
 * * List<Product> findAllByOrdersCount(int ordersCount); - найти все товары по количеству заказов, в которых участвуют
 */
public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5433/postgres", "postgres", "");
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        ProductsRepository productsRepository = new ProductsRepositoryImpl(dataSource);

//        Product product = Product.builder()
//                .description("Электрический чайник SCARLETT SC25")
//                .cost(500)
//                .count(2)
//                .build();
//        productsRepository.save(product);

        System.out.println(productsRepository.findAllByPrice(28990));
        System.out.println(productsRepository.findAllByOrdersCount(3));
    }
}
