package com.alina;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;


public class ProductsRepositoryImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into product (description, cost, count) " +
            "values (?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product";

    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from product where cost = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ORDER_COUNT =
            "select o.product_id id, min(p.description) description, min(p.cost) cost, min(p.count) count, " +
                    "count(*) as cnt " +
            "from orders o " +
            "inner join product p on o.product_id = p.id " +
            "group by o.product_id " +
            "having count(*) = ?";

    private JdbcTemplate jdbcTemplate;

    public ProductsRepositoryImpl(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double cost = row.getDouble("cost");
        int count = row.getInt("count");

        return new Product(id, description, cost, count);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public void save(Product product){
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getCost(), product.getCount());
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, productRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_SELECT_BY_ORDER_COUNT, productRowMapper, ordersCount);
    }
}
