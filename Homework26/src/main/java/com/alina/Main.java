package com.alina;

public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();

        System.out.println(Math.sqrt(31));
        System.out.println(numbersUtil.isPrime(13)); // true
        System.out.println(numbersUtil.isPrime(31)); // true
        System.out.println(numbersUtil.isPrime(169)); // false -> true!!!
        System.out.println(numbersUtil.isPrime(172)); // false
        System.out.println(numbersUtil.isPrime(2)); // true
        System.out.println(numbersUtil.isPrime(3)); // true

        System.out.println(numbersUtil.gcd(18, 12)); //6
        System.out.println(numbersUtil.gcd(9, 12)); //3
        System.out.println(numbersUtil.gcd(64, 48)); //16
    }
}
