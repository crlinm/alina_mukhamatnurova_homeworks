package Homework05;
import java.util.Scanner;


class Main{
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // инициализация начального значения минимальной цифры среди введённых чисел
        int minDigit = -1;

        // цикл пока не равно -1
        while (true) {
            // считываем число с клавиатуры
            int a = scanner.nextInt();

            // выходим из цикла, если число равно -1
            if (a == -1) {
                break;
            }

            a = Math.abs(a);
            while (a != 0) {
                int lastDigit = a % 10;
                if (minDigit < 0 || lastDigit < minDigit) {
                    minDigit = lastDigit;
                }
                a /= 10;
            }
        }

        // выводим минимальную цифру среди введённых чисел
        System.out.println(minDigit);

    }
}