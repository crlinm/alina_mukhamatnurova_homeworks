import java.util.Scanner;

public class Homework07{
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] countArray = new int[201]; // массив с количеством ввода соответствующего числа +100
        int minCount = Integer.MAX_VALUE; // инициализируем минимальное значение максимально возможным
        int minCountIndex = -1; // индекс искомого числа в массие с количествами

        // цикл пока не равно -1
        while (true) {
            // считываем число с клавиатуры
            int number = scanner.nextInt();

            // выходим из цикла, если число равно -1
            if (number == -1 || number > 100 || number < -100) {
                break;
            }

            countArray[number+100]++;
        }

        // ищем число, которое присутствует в последовательности минимальное количество раз
        for (int i = 0; i < countArray.length; i++) {
            if (countArray[i]<minCount && countArray[i]>0){
                minCount = countArray[i];
                minCountIndex = i;
            }
        }

        // выводим число, которое присутствует в последовательности минимальное количество раз
        System.out.println(minCountIndex-100 + " ");
    }
}