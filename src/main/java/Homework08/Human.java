package Homework08;

public class Human {
    private float weight;
    private String name;

    public String getName(){
        return this.name;
    }

    public float getWeight(){
        return this.weight;
    }

    public void setWeight(float weight){
        if (weight < 0){
            weight = 0;
        }
        this.weight = weight;
    }

    public void setName(String name){
        this.name = name;
    }
}
