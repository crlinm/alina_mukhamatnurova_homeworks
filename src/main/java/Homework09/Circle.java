package Homework09;

public class Circle extends Ellipse{
    public Circle(double x, double y, double radius) {
        super(x, y, radius, radius);
    }

    /**
     * Получение периметра окружности
     * @return - возвращает периметр окружности
     */
    public double getPerimeter() {
        return 2 * Math.PI * getRadius1();
    }
}
