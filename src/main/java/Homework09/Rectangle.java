package Homework09;

public class Rectangle extends Figure{
    private double a;
    private double b;

    public Rectangle(double x, double y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    /**
     * Получение периметра прямоугольника
     * @return - возвращает периметр прямоугольника
     */
    public double getPerimeter() {
        return 2 * a + 2 * b;
    }
}
