package Homework10;

/**
 * Сделать класс Figure из задания 09 абстрактным.
 * Определить интерфейс, который позволит перемещать фигуру на заданные координаты.*
 * Данный интерфейс должны реализовать только классы Circle и Square.*
 * В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
 */
public class Main {
    public static void main(String[] args) {
        double xDestination = 1.0;
        double yDestination = 2.0;

        /**
         * массив "перемещаемых" фигур
         */
        Movable[] movables = new Movable[2];

        Rectangle rectangle = new Rectangle(0,0,4,5);
        Ellipse ellipse = new Ellipse(0, 0, 2, 1);
        Square square = new Square(0,0, 4);
        Circle circle = new Circle(0, 0, 2);

        movables[0] = circle;
        movables[1] = square;

        /**
         * Фигуры из массива сдвигаю в конкретную точку (xDestination, yDestination)
         */
        for (int i = 0; i < movables.length; i++) {
            System.out.println(movables[i] + " from Point: (" + circle.getX() + ", " + circle.getY() + ")");
            movables[i].moveTo(xDestination, yDestination);
            System.out.println(movables[i] + " has been moved Point to: (" + circle.getX() + ", " + circle.getY() + ")");
        }

        System.out.println(rectangle + " perimeter: " + rectangle.getPerimeter());
        System.out.println(square + " perimeter: " + square.getPerimeter());
        System.out.println(ellipse + " perimeter: " + ellipse.getPerimeter());
        System.out.println(circle + " perimeter: " + circle.getPerimeter());
    }
}
