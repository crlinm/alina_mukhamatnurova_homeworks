package Homework10;

public class Rectangle extends Figure{
    private double a;
    private double b;

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public Rectangle(double x, double y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    /**
     * Получение периметра прямоугольника
     * @return - возвращает периметр прямоугольника
     */
    public double getPerimeter() {
        return 2 * a + 2 * b;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " (side1: " + a + ", side2: " + b + ")";
    }
}
