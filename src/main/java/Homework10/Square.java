package Homework10;

public class Square extends Rectangle implements Movable {
    public Square(double x, double y, double a) {
        super(x, y, a, a);
    }

    @Override
    public void moveTo(double x, double y) {
        this.setX(x);
        this.setY(y);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " (side: " + getA() + ")";
    }
}
