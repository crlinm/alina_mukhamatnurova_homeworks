package Homework13;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {123, 212, 32, 41, 52};
        System.out.println(Arrays.toString(array));

        /**
         * проверка на четность элемента
         */
        int[] result = Sequence.filter(array, number -> (number % 2 == 0));
        System.out.println(Arrays.toString(result));

        /**
         * проверка, является ли сумма цифр элемента четным числом.
         */
        int[] result2 = Sequence.filter(array, number -> {
                    int sum = 0;
                    while (number != 0){
                        sum += number % 10;
                        number /= 10;
                    }
                    return (sum % 2 == 0);
                }
        );
        System.out.println(Arrays.toString(result2));
    }
}
