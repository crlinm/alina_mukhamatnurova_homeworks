package Homework13;

import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int[] result = new int[array.length];
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])){
                result[count] = array[i];
                count++;
            }
        }
        return Arrays.stream(result).limit(count).toArray();
    }
}
