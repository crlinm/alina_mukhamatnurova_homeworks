package Homework16;

//import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(33);
        numbers.add(15);
        numbers.add(11);
        numbers.add(89);
        numbers.add(17);
        numbers.removeAt(3);
        numbers.add(5);

        LinkedList<Integer> list = new LinkedList<>();
        list.add(34);
        list.add(120);
        list.add(-10);
        list.add(11);
        list.add(50);
        list.add(100);
        list.add(99);
        list.addToBegin(77);
        list.addToBegin(88);
        list.addToBegin(99);

        System.out.println(list.get(3));
        System.out.println(list.get(33));
        System.out.println(list.get(0));

    }
}
