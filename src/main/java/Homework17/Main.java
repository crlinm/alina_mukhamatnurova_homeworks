package Homework17;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();

        String[] words = string.split(" ");

        Map<String, Integer>  map = mergeMap(words);
        printMap(map, "mergeMap");

        Map<String, Integer>  map2 = computeMap(words);
        printMap(map2, "computeMap");

        Map<String, Integer>  map3 = ternaryMap(words);
        printMap(map3, "ternaryMap");
    }

    private static void printMap(Map<String, Integer> map, String methodName) {
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        System.out.println(methodName);
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Слово - " + entry.getKey() + ", количество раз - " + entry.getValue());
        }
        System.out.println();
    }

    private static Map<String, Integer> ternaryMap(String[] words){
        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            if (word.equals("")) continue;
            Integer currentValue = map.get(word);
            map.put(word, currentValue != null ? currentValue + 1 : 1);
        }
        return map;
    }

    private static Map<String, Integer> computeMap(String[] words){
        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            if (word.equals("")) continue;
            map.compute(word, (w, prev) -> prev != null ? prev + 1 : 1);
        }
        return map;
    }

    private static Map<String, Integer>  mergeMap(String[] words){
        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            if (word.equals("")) continue;
            map.merge(word, 1,  (a, b) -> a + 1);
        }
        return map;
    }
}
