package Homework19;

import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();

        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        System.out.println();

//        User user = new User("Sergey", 33, true);
//        usersRepository.save(user);

        List<User> usersByAge = usersRepository.findByAge(33);
        for (User userByAge : usersByAge) {
            System.out.println(userByAge.getAge() + " " + userByAge.getName() + " " + userByAge.isWorker());
        }

        System.out.println();

        List<User> usersIsWorkerIsTrue = usersRepository.findByIsWorkerIsTrue();
        for (User userIsWorkerIsTrue : usersIsWorkerIsTrue) {
            System.out.println(userIsWorkerIsTrue.getAge() + " " + userIsWorkerIsTrue.getName() + " " + userIsWorkerIsTrue.isWorker());
        }
    }
}
