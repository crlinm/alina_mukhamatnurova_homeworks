package Homework20;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CarsRepositoryImpl implements CarsRepository {

    private String fileName;

    public CarsRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Car> findAll() throws LoadCarException {
        List<Car> cars = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                String idCar = parts[0];
                String model = parts[1];
                String color = parts[2];
                int km = Integer.parseInt(parts[3]);
                int cost = Integer.parseInt(parts[4]);
//                double cost = Double.parseDouble(parts[4]);
                Car newCar = new Car(idCar, model, color, km, cost);
                cars.add(newCar);
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new LoadCarException(fileName, e);
        }
        return cars;
    }

    @Override
    public void save(Car car) throws SaveCarException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write(car.getIdCar() + "|" + car.getModel() + "|" + car.getColor()
                    + "|" + car.getKm() + "|" + car.getCost());
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            throw new SaveCarException(fileName, e);
        }
    }

}
